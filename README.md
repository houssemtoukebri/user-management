# User-management

This application is packaged as a war which has Tomcat 8 embedded. No Tomcat or JBoss installation is necessary.

## Requirements
JDK 1.8 

Maven 3

## Getting started

clone a copy on your local

- git clone https://gitlab.com/houssemtoukebri/user-management.git

## Install and run

build and run test by using:

-mvn clean package

Next, you can run the application by executing:

-java -jar target/users-management-0.0.1-SNAPSHOT.jar

The application will be accessible at http://localhost:8081.




