package com.user.management.usersmanagement.controllers;

import com.user.management.usersmanagement.dtos.UserDTO;
import com.user.management.usersmanagement.exceptions.IllegalCountryException;
import com.user.management.usersmanagement.exceptions.IllegalPhoneNumberException;
import com.user.management.usersmanagement.exceptions.InvalidDateFormatException;
import com.user.management.usersmanagement.exceptions.MissingRequiredDataException;
import com.user.management.usersmanagement.exceptions.UserInvalidCountryOrAgeException;
import com.user.management.usersmanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/get-all-users")
    public ResponseEntity getAllUsers() {

        if (userService.getAllUsers() == null || userService.getAllUsers().isEmpty()) {
            return ResponseEntity.badRequest().body("No Users in database");
        }
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @PostMapping("/add-user")
    public ResponseEntity addUser(@RequestBody UserDTO userDTO) {
        try {
            return ResponseEntity.ok(userService.saveUser(userDTO));
        } catch (IllegalCountryException | IllegalPhoneNumberException | MissingRequiredDataException | InvalidDateFormatException | UserInvalidCountryOrAgeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
