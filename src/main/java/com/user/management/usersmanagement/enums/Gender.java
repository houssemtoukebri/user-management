package com.user.management.usersmanagement.enums;

/**
 *
 * @author toukebri.houssem
 */
public enum Gender {
    MALE,FEMALE
}
