package com.user.management.usersmanagement.exceptions;

public class IllegalCountryException extends Exception{
    public IllegalCountryException(String message){
        super(message);
    }
}
