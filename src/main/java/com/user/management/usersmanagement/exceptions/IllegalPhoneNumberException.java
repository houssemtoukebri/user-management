package com.user.management.usersmanagement.exceptions;

public class IllegalPhoneNumberException extends Exception{
    public IllegalPhoneNumberException(String message){
        super(message);
    }
}
