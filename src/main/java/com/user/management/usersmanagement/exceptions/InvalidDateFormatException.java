package com.user.management.usersmanagement.exceptions;

public class InvalidDateFormatException extends Exception{
    public InvalidDateFormatException(String message){
        super(message);
    }
}
