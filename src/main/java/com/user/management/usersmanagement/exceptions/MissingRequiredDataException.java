/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.user.management.usersmanagement.exceptions;


public class MissingRequiredDataException extends Exception{

    public MissingRequiredDataException(String message) {
        super(message);
    }  
}
