package com.user.management.usersmanagement.exceptions;

public class UserInvalidCountryOrAgeException extends Exception{
    public UserInvalidCountryOrAgeException(String message){
        super(message);
    }
}
