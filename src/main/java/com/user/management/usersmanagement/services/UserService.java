package com.user.management.usersmanagement.services;

import com.user.management.usersmanagement.dtos.UserDTO;
import com.user.management.usersmanagement.exceptions.IllegalCountryException;
import com.user.management.usersmanagement.exceptions.IllegalPhoneNumberException;
import com.user.management.usersmanagement.exceptions.InvalidDateFormatException;
import com.user.management.usersmanagement.exceptions.MissingRequiredDataException;
import com.user.management.usersmanagement.exceptions.UserInvalidCountryOrAgeException;
import com.user.management.usersmanagement.models.User;

import java.util.List;

public interface UserService {

    User saveUser(UserDTO userDTO) throws IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException, MissingRequiredDataException;

    List<User> getAllUsers();
}
