package com.user.management.usersmanagement.services.impl;

import com.user.management.usersmanagement.dtos.UserDTO;
import com.user.management.usersmanagement.enums.Gender;
import com.user.management.usersmanagement.exceptions.IllegalCountryException;
import com.user.management.usersmanagement.exceptions.IllegalPhoneNumberException;
import com.user.management.usersmanagement.exceptions.InvalidDateFormatException;
import com.user.management.usersmanagement.exceptions.MissingRequiredDataException;
import com.user.management.usersmanagement.exceptions.UserInvalidCountryOrAgeException;
import com.user.management.usersmanagement.models.User;
import com.user.management.usersmanagement.repositories.UserRepository;
import com.user.management.usersmanagement.services.UserService;
import com.user.management.usersmanagement.utils.UserManagementHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.MessageFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    private static final String DATE_PATTERN = "dd/MM/yyyy";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    private static final int AGE_YEARS_LIMIT = 18;
    private static final String FRENCH_COUNTRY = "France";

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(UserDTO userDTO) throws IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException, MissingRequiredDataException {
        if (userDTO.getUserName() == null || userDTO.getUserName().isEmpty()
                || userDTO.getCountryOfResidence() == null || userDTO.getCountryOfResidence().isEmpty()
                || userDTO.getBirthDate() == null) {
            throw new MissingRequiredDataException("Please enter all the required user parameters");
        }
        if (!UserManagementHelper.isCountryValid(userDTO.getCountryOfResidence())) {
            throw new IllegalCountryException("Country not found, Please correct country name");
        }
        if (userDTO.getPhoneNumber() != null && !UserManagementHelper.isPhoneNumberValid(userDTO.getPhoneNumber())) {
            throw new IllegalPhoneNumberException("Please correct phone number");
        }
        try {
            DATE_TIME_FORMATTER.parse(userDTO.getBirthDate());
        } catch (DateTimeException ex) {
            throw new InvalidDateFormatException(MessageFormat.format("Illegal date format. Date format should be : {0}.", DATE_PATTERN));
        }
        LocalDate birthDate = LocalDate.parse(userDTO.getBirthDate(), DATE_TIME_FORMATTER);
        Period age = Period.between(birthDate, LocalDate.now());
        if (age.getYears() < AGE_YEARS_LIMIT || !FRENCH_COUNTRY.equals(userDTO.getCountryOfResidence())) {
            throw new UserInvalidCountryOrAgeException(MessageFormat.format("Age should be greater than {0} and country should be {1}.", AGE_YEARS_LIMIT, FRENCH_COUNTRY));
        }
        Gender gender = null;
        if (userDTO.getGender() != null) {
            gender = Gender.valueOf(userDTO.getGender());
        }
        User user = User.builder()
                .userName(userDTO.getUserName())
                .gender(gender)
                .countryOfResidence(userDTO.getCountryOfResidence())
                .birthDate(birthDate)
                .phoneNumber(userDTO.getPhoneNumber())
                .build();
        return saveUser(user);
    }
    private User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

}
