package com.user.management.usersmanagement.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class UserManagementHelper {

    public static final List<String> countriesList = constructCountryList();
    public static final String DEFAULT_LOCALE ="en";

    private UserManagementHelper() {
        // private constructor to enforce no instantiation of this *helper* class
    }

    private static List<String> constructCountryList() {
        List<String> isoCountries = Arrays.asList(Locale.getISOCountries());
        return isoCountries.stream().map(isoCountry -> {
            Locale locale = new Locale(DEFAULT_LOCALE, isoCountry);
            return locale.getDisplayCountry();
        }).collect(Collectors.toList());
    }

    public static boolean isCountryValid(String countryName) {
        return countriesList.contains(countryName);
    }

    public static boolean isPhoneNumberValid(String phoneNumber){
        Pattern pattern = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$");
        Matcher matcher=pattern.matcher(phoneNumber);
        return matcher.matches();
    }
}
