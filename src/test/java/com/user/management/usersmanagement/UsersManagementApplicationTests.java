package com.user.management.usersmanagement;

import com.user.management.usersmanagement.dtos.UserDTO;
import com.user.management.usersmanagement.exceptions.IllegalCountryException;
import com.user.management.usersmanagement.exceptions.IllegalPhoneNumberException;
import com.user.management.usersmanagement.exceptions.InvalidDateFormatException;
import com.user.management.usersmanagement.exceptions.MissingRequiredDataException;
import com.user.management.usersmanagement.exceptions.UserInvalidCountryOrAgeException;
import com.user.management.usersmanagement.services.UserService;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
class UsersManagementApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    void testSaveUser() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .userName("Houssem")
                .gender("MALE")
                .countryOfResidence("France")
                .birthDate("10/03/1995")
                .phoneNumber("+33 9876543210")
                .build();
        userService.saveUser(user);

    }

    @Test()
    void testMissingRequiredData() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .gender("MALE")
                .countryOfResidence("France")
                .birthDate("10/03/1995")
                .phoneNumber("+33 9876543210")
                .build();

        Exception exception = assertThrows(Exception.class, () -> {
            userService.saveUser(user);
        });
        Assertions.assertTrue(exception instanceof MissingRequiredDataException);
    }

    @Test()
    void testIllegalCountry() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .userName("Houssem")
                .gender("MALE")
                .countryOfResidence("Francee")
                .birthDate("10/03/1995")
                .phoneNumber("+33 9876543210")
                .build();

        Exception exception = assertThrows(Exception.class, () -> {
            userService.saveUser(user);
        });
        Assertions.assertTrue(exception instanceof IllegalCountryException);
    }

    @Test()
    void testIllegalPhoneNumber() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .userName("Houssem")
                .gender("MALE")
                .countryOfResidence("France")
                .birthDate("10/03/1995")
                .phoneNumber("+3a3 9876543210")
                .build();

        Exception exception = assertThrows(Exception.class, () -> {
            userService.saveUser(user);
        });
        Assertions.assertTrue(exception instanceof IllegalPhoneNumberException);
    }

    @Test()
    void testIllegalDateFormat() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .userName("Houssem")
                .gender("MALE")
                .countryOfResidence("France")
                .birthDate("10/03/19956")
                .phoneNumber("+33 9876543210")
                .build();

        Exception exception = assertThrows(Exception.class, () -> {
            userService.saveUser(user);
        });
        Assertions.assertTrue(exception instanceof InvalidDateFormatException);
    }

    @Test
    void testInvalidCountryOrAge() throws MissingRequiredDataException, IllegalCountryException, IllegalPhoneNumberException, InvalidDateFormatException, UserInvalidCountryOrAgeException {
        UserDTO user = UserDTO.builder()
                .userName("Houssem")
                .gender("MALE")
                .countryOfResidence("France")
                .birthDate("10/03/2005")
                .phoneNumber("+33 9876543210")
                .build();
        Exception exception = assertThrows(Exception.class, () -> {
            userService.saveUser(user);
        });
        Assertions.assertTrue(exception instanceof UserInvalidCountryOrAgeException);
    }

    @Test
    @Sql({"/import_users.sql"})
    void testgetAllUsers() {
        Assertions.assertEquals(2, userService.getAllUsers().size());

    }
}
